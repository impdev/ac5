import unittest
from app import app, session

class test_app(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()


    ## test_<routes>

    # GET
    def test_home(self):
        r = self.app.get('/home')
        self.assertEqual( 200, r.status_code )



    # GET+POST
    def test_cadastro(self):
        dados = {"email" : "test@unit.com" ,
                 "nome" : "Test", 
                 "senha" : "Unit"           }
        
        r1 = self.app.get('/cadastro')
        r2 = self.app.post('/cadastro', data = dados)

        self.assertEqual( 200, r1.status_code )
        self.assertEqual( 302 , r2.status_code)
        self.assertIn(dados["email"], session['usuarios'])

    def test_login(self):
        dados = {"email" : "test@unit.com",
                 "senha" : "Unit"}
        
        r1 = self.app.get('/login')
        r2 = self.app.post('/login', data = dados)

        self.assertEqual( 200, r1.status_code )
        self.assertEqual( 302, r2.status_code )

    # REDIRECTS
    def test_area_logada(self):
        r = self.app.get('/area_logada')
        self.assertEqual( 302 , r.status_code )

    def test_logout(self):
        r = self.app.get('/logout')
        self.assertEqual(302, r.status_code)


if __name__ == "__main__":
    unittest.main()