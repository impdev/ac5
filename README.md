
# TemperoDaFe - Projeto Flask em Docker e serviços AWS(Academy Tier)

Projeto de uma página WEB utilizando framework Flask em container Docker, com estrutura CI/CD no gitlab com runner em nuvem (AWS-EC2) e deploy (AWS-ECS). Com objetivo de compreender conceitos de DevOps, como a estrutura cíclica de planejamento -> desenvolvimento -> Aplicação -> Monitoramento. 


## Pré-requisitos

Certifique-se de que você tenha os seguintes pré-requisitos instalados em sua máquina para testes locais:

- Docker: [Instalação do Docker](https://docs.docker.com/get-docker/)
- Wsl: [Instalação do WSL](https://learn.microsoft.com/pt-br/windows/wsl/install)


## Configuração

Faça um Fork do projeto [Tempero da Fe](https://gitlab.com/impdev/ac4) e clone para um repositório local


## Executando o aplicativo com Docker 

Execute o seguinte comando para construir a imagem e container do aplicativo usando Docker:

```bash
docker build -t ac4 .
docker run -dp 5000:5000 ac4
```

Para parar o container, você pode usar o seguinte comando, use "ls" para pegar o Id do container,
depois utilize o "stop" para parar o docker:

```bash
docker container ls
docker container stop <IdContainer>
```


## Página WEB

Acesse o aplicativo em [http://localhost:5000](http://localhost:5000) no seu navegador. Você pode se registrar, fazer login.


## AWS EC2 Instância + GitLab Runner

Configuração do GitLab runner usando uma instância EC2 (AWS), para execução dos testes unitários e testes de vulnerabilidade de código (SAST).

### Criar e conectar instância EC2
Acesse e siga a Etapa 1 para criar uma instancia [EC2 Linux](https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/EC2_GetStarted.html), guarde o par de chaves para conexão.

Conecte-se a instância por meio do [puTTY](https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/putty.html):
- Session: Host name -> Ip público da instância EC2
- Session>SSH>Auth: Credentials -> Selecionar o par de chaves na máquina local
- No terminal, logar como: "ubuntu"

### Configurações e requisitos

No terminal puTTY:

Instalação e Login Docker, após commando "login" informar usuário e senha
```bash
sudo apt update
sudo apt install docker.io
sudo docker login
```

Configuração GitLab-Runner, consiste em baixar e instalar o pacote de instalação debian do gitlab-runner usando a arquitetura (arch) amd64
```bash
curl -LJO "https://s3.amazonaws.com/gitlab-runner-downloads/v15.10.1/deb/gitlab-runner_amd64.deb"
sudo dpkg -i gitlab-runner_amd64.deb
```

Registro do Runner, sequência de parâmentros para registro de um novo runner
```bash
sudo gitlab-runner register
```
Inserir na sequência:

- URL: "https://gitlab.com/"
- Token: Acesse no GitLab o grupo do projeto -> Click na aba lateral Build>Runners -> 3 pontos (superior direita) copie e cole o Registration token
- Descrição do Runner
- Tags (Opcional)
- Notas de Manutenção (Opicional)
- Executor: "docker"
- Imagem: "python"


### Testes e artefatos

No GitLab acesse o projeto -> Click na aba lateral build>Pipelines. Caso não tenha iniciado de forma automática, click em Run Pipeline ->Run Pipeline.

Na coluna stages ao clickar no marcador é possível observar os estágios e jobs, acessando-os visulize o terminal com as instruções executadas. 

Ao lado de stages há um ícone de download onde é diponibilizado os artefatos da pipeline.


## AWS ECS Cluster

Configuração de integração CI/CD entre ECS e gitlab.

### Config gitlab-runner
No terminal de acesso do puTTY digite o seguinte comando para alterar as configurações do gitlab-runner
```bash
sudo nano /etc/gitlab-runner/config.toml
```
com as setas navegue até "privileged" altere o valor para "true" e execute a sequência de teclas para confirmar a alteração, ctrl + x ,   y, enter.

### Template ECS
No arquivo .gitlab-ci.yml, na chave include, adicionamos mais um tópico com " - template: AWS/Deploy-ECS.gitlab-ci.yml " o template padrão da AWS-ECS com gitlab

### ECS 
Foi criado uma TASK DEFINITION com parâmetros:

- Nome da Task
- Launch type: Fargate
- OS/Arch: Linux/x86_64
- Task size: .5 vCPU , 2 GB memory
- Image URI: encontrada no Projeto do gitlab-> Deploy -> Container Registry, no icone de copiar o path.

-ADD envoirement variable -> Key: secret_key, Value: insira uma senha segura para encriptar senhas no arquivo do framework FLASK (app.py)

- Loggin: Habilitado log collection 


Parâmetros de criação do CLUSTER na aba do ECS:

- Nome do Cluster
- remover namespace

- Infra: Fargate(serverless)

- Monitoring: Habilitar Use Container Insights


No cluster criado adcionamos o SERVICE:
- Family: sua Task definition
- Revision: latest
- Nome do serviço

- Networking
- VPC: Padrão
- selecione apenas 2 subnets
- inclua um grupo de segurança padrão

- Load Balancer: Application Load Balancer
- Load Balancer name
- Target group name

### Security Group
Adicionar tráfego de entrada HTTP:
Procure na barra de pesquisa da AWS por Security groups, selecione o grupo de segurança do Cluster > Click no botão "edit Inbound Roules"
adicione uma nova regra:
- Type: HTTP
- inclua o ip: 0.0.0.0/0

salve as alterações

## CD no GitLab
Após configuração do ECS vamos adicionar varíaveis de ambiente no projeto do GitLab, para manter o cíclo de atualização contínua:
No projeto do Git lab acesse Settings > CI/CD > Expand Variables.
Adicione as seguintes variáveis :

Encontrado no página de início do laboratório (vocareum) clicando em iAWS Details 
- AWS_ACCESS_KEY_ID 
- AWS_SECRET_ACCESS_KEY
- AWS_SESSION_TOKEN

Encontrado na página inicial do laboratório (site AWS), ao lado do USERNAME, por padrão "us-east-1"
- AWS_DEFAULT_REGION

Nomes escolhidos durante a configuração do ECS
- CI_AWS_ECS_CLUSTER
- CI_AWS_ECS_SERVICE
- CI_AWS_ECS_TASK_DEFINITION

## Acesso a página WEB
Após a configuração das variáveis e retorno positivo da pipeline do projeto,
podemos acessar a página disponível na internet usando o DNS do Serviço criado:
ECS > Cluster > Service: Aba Network > DNS Names

## Contribuição

Integrantes:


      Nome: Hygor Rodrigues Cesconetto
      RA : 2203138
 
      Nome Ewerton Ferreira Costa
      RA : 2202829

      Nome : Pedro Luis Da Paz Dos Santos
      RA: 2202636

      Nome : João Victor Machado Maniezzo
      RA : 2202323