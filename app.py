import os
from flask import Flask

app = Flask(__name__)

from controllers import *

app.secret_key = os.environ.get("secret_key")

if __name__ == "__main__":
    app.run(host='0.0.0.0')